/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 13:23:04 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 19:49:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdlib.h>

/*
**	Binary (b)
**		Prints the number in binary format.
**		Support field width and presicion as ussual number.
**
**	ERRNO check (m, M)
**		Prints the ERRNO status, if ERRNO is 0 - prints "OK"
**		M (big m):		prints message "ERRNO is " and ERRNO code
**		m (small m):	prints OK or the ERRNO code
**
**	Colorful output (r)
**		Enables colorization for the text being printed after the flag.
**		Colors support:
**			Light gray, Light grey, Dark gray, Dark grey
**			Black, White, Red, Green, Blue, Magenta, Yellow, Cyan
**		Additional stuff support(depends on the terminal you're using)
**			blinking, underlined, dim, bold, bright, reverse, hidden
**		Usage:
**			ft_printf("%rTEXT", "red")
**			ft_printf("%rTEXT", "rEd")
**			ft_printf("%rTEXT", "kljdsfwl933-red2342")
**			ft_printf("%rTEXT", "RED")	- will print <TEXT> in red
**
**			ft_printf("%rTEXT", "sdfsd blinking555555white33 33dfa")
**			ft_printf("%rTEXT", "blinking white")
**										- will print <TEXT>
**						in white and blinking(if terminal supports)
**
**	Since its using custom conversions, it is impossible(almost) to use
**		__attribute__((format (printf, 1, 2))) here.
*/

/*
**	If you want to have color defines in your code, just copy these:
**	# define COLOR_RESET	"\x1b[0m"
**	# define COLOR_BLACK	"\x1b[30m"
**	# define COLOR_RED		"\x1b[31m"
**	# define COLOR_GREEN	"\x1b[32m"
**	# define COLOR_YELLOW	"\x1b[33m"
**	# define COLOR_BLUE		"\x1b[34m"
**	# define COLOR_MAGENTA	"\x1b[35m"
**	# define COLOR_CYAN		"\x1b[36m"
**	# define COLOR_WHITE	"\x1b[37m"
**	# define COLOR_L_GRAY	"\x1b[38m"
**	# define COLOR_D_GRAY	"\x1b[90m"
*/

size_t			ft_printf(const char *command_str, ...);

#endif
