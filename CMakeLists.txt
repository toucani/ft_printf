cmake_minimum_required(VERSION 3.5 FATAL_ERROR)

project(ftprintf VERSION 1.4.0.0
        DESCRIPTION "My own printf implementation"
        LANGUAGES C)

#########################
#   Setting defaults    #
#########################

set(CMAKE_C_STANDARD 11)

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
    message(STATUS "Setting build type to '${CMAKE_BUILD_TYPE}' as none was specified.")
endif()

#####################
#   Library rules   #
#####################

add_library(${PROJECT_NAME} STATIC
        sources/ft_cnv_compute.c
        sources/ft_cnv_m_clr.c
        sources/ft_cnv_m_nmb.c
        sources/ft_cnv_m_spc.c
        sources/ft_cnv_m_str.c
        sources/ft_cnv_main.c
        sources/ft_fts_field.c
        sources/ft_printf.c)

add_subdirectory(externals/libft)

target_link_libraries(${PROJECT_NAME} PUBLIC ft)

target_compile_options(${PROJECT_NAME} PUBLIC
        -Wall -Wextra -Werror -Wstrict-prototypes -fverbose-asm -fpic)

target_compile_definitions(${PROJECT_NAME} PUBLIC
	_REENTRANT)

target_include_directories(${PROJECT_NAME} PUBLIC includes)

set_target_properties(${PROJECT_NAME} PROPERTIES
        VERSION ${PROJECT_VERSION}
        PUBLIC_HEADER includes/ft_printf.h)

#################
#   Sanitizers  #
#################

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/externals/sanitizers4cmake/cmake" ${CMAKE_MODULE_PATH})
find_package(Sanitizers)
add_sanitizers(ftprintf)
