/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cnv_m_nmb.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/29 16:53:39 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Functions to form the output for integers
**	fts - name of all conversion functions
**	l/b - little or big letter of conversion (l_s = s, b_s = S)
*/

void	fts_l_d(t_conv_info *info)
{
	info->data = ft_itoa(ft_fts_data_get(info));
	ft_fts_isnull(info);
	if ((info->data[0] == '-') && (info->add = ft_strdup("-")))
		ft_memmove(info->data, &info->data[1], ft_strlen(info->data));
	if (!info->add &&
		(ft_strchr(info->args, '+') || ft_strchr(info->args, ' ')))
		info->add = ft_strdup((ft_strchr(info->args, '+')) ? "+" : " ");
}

void	fts_l_u(t_conv_info *info)
{
	info->data = ft_uitoa(ft_fts_data_get(info));
	ft_fts_isnull(info);
}

void	fts_l_x(t_conv_info *info)
{
	info->data = ft_uitoa_base(ft_fts_data_get(info), 16,
					info->cnv_char);
	if (ft_strchr(info->args, '#') && !ft_strequ(info->data, "0"))
	{
		info->add = ft_strdup("0x");
		info->add[1] = info->cnv_char;
	}
	ft_fts_isnull(info);
}

void	fts_l_o(t_conv_info *info)
{
	info->data = ft_uitoa_base(ft_fts_data_get(info), 8, 'a');
	ft_fts_isnull(info);
	if (ft_strchr(info->args, '#') && !ft_strequ(info->data, "0"))
		info->add = ft_strdup("0");
}
