/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cnv_m_spc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/29 20:55:47 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Functions to get the stuff for special stuff
**	fts - name of all conversion functions
**	l/b - little or big letter of conversion (l_s = s, b_s = S)
*/

void		fts_l_p(t_conv_info *info)
{
	info->data = ft_uitoa_base(ft_fts_data_get(info), 16, 'a');
	info->add = ft_strdup("0x");
	if (ft_strequ(info->data, "0") && ft_strchr(info->args, '.'))
		info->data[0] = 0;
}

void		fts_l_b(t_conv_info *info)
{
	info->data = ft_uitoa_base(ft_fts_data_get(info), 2, 'a');
}

void		fts_l_prc(t_conv_info *info)
{
	info->data = ft_strdup("%");
}
