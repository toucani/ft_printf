/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cnv_main.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 11:56:25 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Initializing array of pointers to conversions.
*/

static void			ft_conversions_init(void (*conversions[CONV_AM])
	(t_conv_info *info))
{
	conversions[0] = fts_l_s;
	conversions[1] = fts_b_s;
	conversions[2] = fts_l_p;
	conversions[3] = fts_l_d;
	conversions[4] = fts_l_d;
	conversions[5] = fts_l_d;
	conversions[6] = fts_l_o;
	conversions[7] = fts_l_o;
	conversions[8] = fts_l_u;
	conversions[9] = fts_l_u;
	conversions[10] = fts_l_x;
	conversions[11] = fts_l_x;
	conversions[12] = fts_l_c;
	conversions[13] = fts_b_c;
	conversions[14] = fts_l_b;
	conversions[15] = fts_l_m;
	conversions[16] = fts_l_m;
	conversions[17] = fts_l_r;
	conversions[18] = fts_l_prc;
}

/*
**	Clearing the structure at the end of use.
*/

static void			ft_cnv_del(t_conv_info *conv_info)
{
	ft_strdel(&(conv_info->args));
	ft_strdel(&(conv_info->add));
	ft_strdel(&(conv_info->presc));
	ft_strdel(&(conv_info->field));
	ft_strdel(&(conv_info->data));
	ft_memdel((void**)&(conv_info->w_data));
}

/*
**	Initializing the structure with data.
**	All unknown conversions(symbols) are treated like %c with
**	that unknown symbol as data to print. So %4e = %4c and 'e' to print.
*/

static t_conv_info	*ft_cnv_init(char *args, va_list *raw_data)
{
	t_conv_info	*conv_info;
	char		*conv_list;
	void		(*conversions[CONV_AM])(t_conv_info *info);

	ft_conversions_init(conversions);
	conv_list = CONVERSIONS;
	conv_info = (t_conv_info*)ft_memalloc(sizeof(t_conv_info));
	conv_info->args = args;
	conv_info->raw_data = raw_data;
	if (!ft_strchr(conv_list, args[ft_strlen(args) - 1]))
	{
		conv_info->raw_data = 0;
		conv_info->data = ft_strnew(1);
		conv_info->data[0] = args[ft_strlen(args) - 1];
		conv_info->args[ft_strlen(args) - 1] = 'c';
	}
	conv_info->cnv_char = args[ft_strlen(args) - 1];
	conv_info->cnv_ft = conversions
	[(ft_strchr(conv_list, conv_info->cnv_char) - conv_list)];
	return (conv_info);
}

/*
**	Parsing asterics and inserting the data
*/

static void			ft_check_asterics(char **args, va_list *raw_data)
{
	char	*result;
	char	*number;

	while (ft_strchr(*args, '*'))
	{
		number = ft_itoa(va_arg(*raw_data, int32_t));
		result = ft_strnew(ft_strlen(*args) + ft_strlen(number));
		ft_strncat(result, *args, ft_strchr(*args, '*') - *args);
		ft_strcat(result, number);
		ft_strcat(result, ft_strchr(*args, '*') + 1);
		ft_strdel(args);
		ft_strdel(&number);
		*args = result;
	}
}

/*
**	Main function to start working with the conversions.
*/

size_t				ft_cnv_main(const char *args, va_list *raw_data)
{
	size_t		ct;
	t_conv_info	*the_structure;

	if (ft_strlen(args) <= 1 ||
	(!ft_isalpha(args[ft_strlen(args) - 1]) && args[ft_strlen(args) - 1] != '%')
	|| args[ft_strlen(args) - 1] == 'h' || args[ft_strlen(args) - 1] == 'l'
	|| args[ft_strlen(args) - 1] == 'z' || args[ft_strlen(args) - 1] == 'j')
	{
		ft_strdel((char **)&args);
		return (0);
	}
	ft_check_asterics((char**)(&args), raw_data);
	the_structure = ft_cnv_init((char*)args, raw_data);
	ft_cnv_compute(the_structure);
	ct = ft_cnv_print(the_structure);
	ft_cnv_del(the_structure);
	ft_memdel((void**)&the_structure);
	return (ct);
}
