/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cnv_m_clr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 10:35:30 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 17:11:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Color parsing
*/

static void	fts_foreground2(t_conv_info *info)
{
	if (info->data)
		return ;
	if (ft_strstr(info->args, "red"))
		info->data = ft_strdup(COLOR_RED);
	else if (ft_strstr(info->args, "green"))
		info->data = ft_strdup(COLOR_GREEN);
	else if (ft_strstr(info->args, "yellow"))
		info->data = ft_strdup(COLOR_YELLOW);
	else if (ft_strstr(info->args, "blue"))
		info->data = ft_strdup(COLOR_BLUE);
	else if (ft_strstr(info->args, "magenta"))
		info->data = ft_strdup(COLOR_MAGENTA);
	else if (ft_strstr(info->args, "cyan"))
		info->data = ft_strdup(COLOR_CYAN);
	else if (ft_strstr(info->args, "black"))
		info->data = ft_strdup(COLOR_BLACK);
	else if (ft_strstr(info->args, "white") || !info->data)
		info->data = ft_strdup(COLOR_WHITE);
	if (ft_strstr(info->args, "light"))
		info->data[2] = '9';
}

static void	fts_foreground(t_conv_info *info)
{
	ft_strdel(&(info->data));
	if (ft_strstr(info->args, "light gray") ||
		ft_strstr(info->args, "light grey"))
		info->data = ft_strdup(COLOR_L_GRAY);
	else if (ft_strstr(info->args, "dark gray") ||
			ft_strstr(info->args, "dark grey"))
		info->data = ft_strdup(COLOR_D_GRAY);
	else
		fts_foreground2(info);
}

/*
**	Additional stuff parsing
*/

static void	fts_additional(t_conv_info *info)
{
	if (ft_strstr(info->args, "bold") || ft_strstr(info->args, "bright"))
		info->data = ft_strjoin_del_first(&(info->data), "\x1b[1m");
	if (ft_strstr(info->args, "dim"))
		info->data = ft_strjoin_del_first(&(info->data), "\x1b[2m");
	if (ft_strchr(info->args, '_') || ft_strstr(info->args, "underline"))
		info->data = ft_strjoin_del_first(&(info->data), "\x1b[4m");
	if (ft_strstr(info->args, "blink"))
		info->data = ft_strjoin_del_first(&(info->data), "\x1b[5m");
	if (ft_strstr(info->args, "reverse"))
		info->data = ft_strjoin_del_first(&(info->data), "\x1b[7m");
	if (ft_strstr(info->args, "hidden"))
		info->data = ft_strjoin_del_first(&(info->data), "\x1b[8m");
}

/*
**	Main color-conversion function.
*/

void		fts_l_r(t_conv_info *info)
{
	ft_strdel(&(info->args));
	info->args = ft_strdup(va_arg(*info->raw_data, char*));
	if (info->args)
	{
		ft_strtolow(info->args);
		if (ft_strstr(info->args, "reset"))
			info->data = ft_strdup(COLOR_RESET);
		else if (info->args[0])
		{
			fts_foreground(info);
			fts_additional(info);
		}
		ft_putstr(info->data);
		ft_strdel(&(info->data));
	}
	info->data = ft_strnew(0);
}
