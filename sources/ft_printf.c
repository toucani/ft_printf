/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 14:25:33 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Checks if the character can be a conversion.
**	h, l, z and j are not threated as conversions.
*/

static int		ft_is_cnv(const char letter)
{
	return (letter == '%' || (ft_isalpha(letter) &&
	letter != 'l' && letter != 'h' && letter != 'j' && letter != 'z')
	|| (!ft_isalnum(letter) && letter != '.' && letter != '#'
	&& letter != ' ' && letter != '-' && letter != '+' && letter != '*'));
}

/*
**	Function to find the right format to print.
**	Return value:
**		OK = amount of characters we need to skip in order to get
**		to the next piece of text.
**		Len += amount of written characters
*/

static size_t	ft_read_format(const char *command_str,
					va_list *raw_data, size_t *len)
{
	size_t			ct;

	ct = 1;
	while (command_str[ct] && !ft_is_cnv(command_str[ct]))
		ct++;
	if (!command_str[ct] ||
	(!ft_isalpha(command_str[ct]) && command_str[ct] != '%'))
		ct--;
	if (command_str[ct])
		*len += ft_cnv_main(ft_strsub(command_str, 0, ct + 1), raw_data);
	return (ct + 1);
}

/*
**	Main function.
**	Return value: number of printed characters
*/

size_t			ft_printf(const char *command_str, ...)
{
	va_list	arg_list;
	size_t	start;
	size_t	end;
	size_t	len;

	start = 0;
	end = 0;
	len = 0;
	va_start(arg_list, command_str);
	while (command_str[end])
		if (command_str[end] == '%')
		{
			len += write(1, &command_str[start], end - start);
			end += ft_read_format(&command_str[end], &arg_list, &len);
			start = end;
		}
		else
			end++;
	len += write(1, &command_str[start], end - start);
	va_end(arg_list);
	return (len);
}

/*
**	Function to print results of conversions properly
**	Return value: amount of printed characters
*/

size_t			ft_cnv_print(const t_conv_info *info)
{
	size_t	ct;

	ct = 0;
	if (ft_strchr(info->args, '-') || (info->field && info->field[0] == '0'))
	{
		ct += ft_putstr(info->add) + ((info->field && info->field[0] == '0') ?
			ft_putstr(info->field) : 0);
		ct += ft_putstr(info->presc) + ((info->cnv_char == 'c') ?
			ft_putchar(info->data[0]) : ft_putstr(info->data));
		ct += (info->cnv_char == 'C') ?
			ft_putchar(info->w_data[0]) : ft_putwstr(info->w_data);
		ct += (info->field && info->field[0] != '0') ?
			ft_putstr(info->field) : 0;
	}
	else
	{
		ct += ft_putstr(info->field) + ft_putstr(info->add);
		ct += ft_putstr(info->presc) + ((info->cnv_char == 'c') ?
			ft_putchar(info->data[0]) : ft_putstr(info->data));
		ct += (info->cnv_char == 'C') ?
			ft_putchar(info->w_data[0]) : ft_putwstr(info->w_data);
	}
	return (ct);
}
