/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_inside.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 13:23:04 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 19:40:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FT_PRINTF_INSIDE_H
# define FT_PRINTF_INSIDE_H

/*
**	It is internal file.
**	You should include ft_printf.h into your project.
*/

# include "libft.h"
# include <stdarg.h>
# include "ft_printf.h"

# define CONV_AM ft_strlen(CONVERSIONS)
# define CONVERSIONS "sSpdDioOuUxXcCbmMr%"

# define COLOR_RESET	"\x1b[0m"
# define COLOR_BLACK	"\x1b[30m"
# define COLOR_RED		"\x1b[31m"
# define COLOR_GREEN	"\x1b[32m"
# define COLOR_YELLOW	"\x1b[33m"
# define COLOR_BLUE		"\x1b[34m"
# define COLOR_MAGENTA	"\x1b[35m"
# define COLOR_CYAN		"\x1b[36m"
# define COLOR_WHITE	"\x1b[37m"
# define COLOR_L_GRAY	"\x1b[38m"
# define COLOR_D_GRAY	"\x1b[90m"

typedef struct	s_conv_info
{
	char		*args;
	char		*add;
	char		*presc;
	char		*field;
	char		*data;
	wchar_t		*w_data;
	va_list		*raw_data;
	size_t		data_len;
	size_t		field_width;
	size_t		presc_width;
	void		(*cnv_ft)(struct s_conv_info *info);
	char		cnv_char;
}				t_conv_info;

/*
**	Conversions
*/

void			fts_l_d(t_conv_info *info);
void			fts_l_u(t_conv_info *info);
void			fts_l_x(t_conv_info *info);
void			fts_l_o(t_conv_info *info);
void			fts_l_p(t_conv_info *info);
void			fts_l_b(t_conv_info *info);
void			fts_l_c(t_conv_info *info);
void			fts_l_s(t_conv_info *info);
void			fts_l_m(t_conv_info *info);
void			fts_b_s(t_conv_info *info);
void			fts_b_c(t_conv_info *info);
void			fts_l_r(t_conv_info *info);
void			fts_l_prc(t_conv_info *info);

/*
**	Other stuff.
*/

size_t			ft_cnv_main(const char *args, va_list *raw_data);
intmax_t		ft_fts_data_get(const t_conv_info *info);
void			ft_fts_isnull(const t_conv_info *info);
void			ft_cnv_compute(t_conv_info *the_structure);
size_t			ft_cnv_print(const t_conv_info *info);
void			ft_presicion(t_conv_info *info);
void			ft_field(t_conv_info *info);
void			ft_join_data(t_conv_info *info);

#endif
