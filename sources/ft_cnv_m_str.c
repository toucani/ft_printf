/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cnv_m_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 17:12:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"
#include <errno.h>

/*
**	Functions to convert va_arg into strings and chars
**	fts - name of all conversion functions
**	l/b - little or big letter of conversion (l_s = s, b_s = S)
*/

void	fts_l_c(t_conv_info *info)
{
	if (info->data)
		return ;
	info->data = ft_strnew(1);
	info->data[0] = ft_fts_data_get(info);
}

void	fts_l_s(t_conv_info *info)
{
	info->data = va_arg(*(info->raw_data), char *);
	if (info->data)
		info->data = ft_strdup(info->data);
	else
		info->data = ft_strdup("(null)");
}

/*
**	Prints the state of ERRNO
**	If 'M' -> "ERRNO is " + <errno state>
**	If 'm' -> <errno state>
**	"OK" is always printed instead of 0.
*/

void	fts_l_m(t_conv_info *info)
{
	char	*temp;

	temp = ft_itoa(errno);
	info->data =
	ft_strjoin((info->cnv_char == 'M') ? "ERRNO is " : "",
		(errno) ? temp : "OK");
	ft_strdel(&temp);
}

/*
**	Functions to convert va_arg into wide strings and wide chars
**	fts - name of all conversion functions
**	l/b - little or big letter of conversion (l_s = s, b_s = S)
*/

void	fts_b_s(t_conv_info *info)
{
	info->w_data = va_arg(*(info->raw_data), wchar_t *);
	if (info->w_data)
		info->w_data = ft_wstrdup(info->w_data);
	else
		info->data = ft_strdup("(null)");
}

void	fts_b_c(t_conv_info *info)
{
	info->w_data = (wchar_t*)ft_memalloc(sizeof(wchar_t) * 2);
	info->w_data[0] = (wchar_t)va_arg(*(info->raw_data), wchar_t);
}
