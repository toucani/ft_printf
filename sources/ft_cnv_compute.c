/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cnv_compute.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 11:15:14 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Getting data properly
**	Data length depends on the conversion used.
**	Small letters by default get (u)int16_t
**	Big letters by default get (u)int32_t
*/

intmax_t		ft_fts_data_get(const t_conv_info *info)
{
	if (info->cnv_char == 'i' || info->cnv_char == 'd'
	|| info->cnv_char == 'c' || info->cnv_char == 'p')
	{
		if (ft_strchr(info->args, 'z') || ft_strchr(info->args, 'j') ||
			ft_strchr(info->args, 'l') || info->cnv_char == 'p')
			return ((va_arg(*(info->raw_data), intmax_t)));
		else if (ft_strstr(info->args, "hh") && info->cnv_char >= 'a')
			return ((int8_t)(va_arg(*(info->raw_data), int32_t)));
		else if (ft_strchr(info->args, 'h'))
			return ((int16_t)(va_arg(*(info->raw_data), int32_t)));
		return ((va_arg(*(info->raw_data), int32_t)));
	}
	else
	{
		if (ft_strchr(info->args, 'z') || ft_strchr(info->args, 'j') ||
			ft_strchr(info->args, 'l') ||
			(info->cnv_char <= 'Z' && info->cnv_char != 'X'))
			return ((va_arg(*(info->raw_data), uintmax_t)));
		else if (ft_strstr(info->args, "hh") &&
		(info->cnv_char >= 'a' || info->cnv_char == 'X'))
			return ((uint8_t)(va_arg(*(info->raw_data), uint32_t)));
		else if (ft_strchr(info->args, 'h'))
			return ((uint16_t)(va_arg(*(info->raw_data), uint32_t)));
		return ((va_arg(*(info->raw_data), uint32_t)));
	}
}

/*
**	Checks if there are necessary conditions to supress the printing.
**	If the presicion is present and it is 0 and the data is 0
**									- nothing must be printed
*/

void			ft_fts_isnull(const t_conv_info *info)
{
	if (ft_strequ(info->data, "0") && ft_strchr(info->args, '.')
		&& !(ft_isdigit((long)ft_strchr(info->args, '.') - 1) &&
		ft_isdigit((long)ft_strchr(info->args, '.') + 1)))
		info->data[0] = 0;
}

/*
**	Presicion atoi, which looks for the last '.' and then makes atoi
**	if the next character is a digit(but not zero, zero is a flag!)
*/

static size_t	ft_presc_atoi(const char *args)
{
	char	*pres;
	size_t	rt;

	rt = 0;
	if (!args || !(pres = ft_strrchr(args, '.')))
		return (0);
	pres++;
	if (*pres && ft_isdigit(*pres))
		rt = ft_atoi(pres);
	return (rt);
}

/*
**	Field width atoi.
**	The first number after flags is the field width.
**	If there are no numbers with the '-' then atoi takes just the first number.
*/

static size_t	ft_field_atoi(const char *args)
{
	unsigned int	rt;
	unsigned int	temp;
	size_t			ct;
	const char		*flags = " +-#%";

	ct = 0;
	rt = 0;
	while (args[++ct])
	{
		if (ft_strchr(flags, args[ct - 1]) && ft_isdigit(args[ct]))
			if ((temp = ft_atoi(&args[ct])))
				rt = temp;
	}
	return (rt);
}

/*
**	Main computing function.
**	Computes all fields, by running ft's.
**	Doesn't compute anything for 'r' conversion(colors)
**	It is guaranteed that:
**		_args_	is never NULL.
**		_data_	is never NULL(either c_data or w_data).
**		_add_	can be NULL(if there's nothing to add when printing).
**		_field_	can be NULL(if there's nothing to add when printing).
**		_presc_	can be NULL(if there's nothing to add when printing).
*/

void			ft_cnv_compute(t_conv_info *info)
{
	info->cnv_ft(info);
	if (info->cnv_char == 'r')
		return ;
	if (info->w_data)
		info->data_len = ft_wstrlen(info->w_data);
	else
		info->data_len = ft_strlen(info->data);
	info->field_width = ft_field_atoi(info->args);
	info->presc_width = ft_presc_atoi(info->args);
	if (ft_tolower(info->cnv_char) != 'c' && info->cnv_char != '%')
		ft_presicion(info);
	if (ft_tolower(info->cnv_char) == 'c' || info->cnv_char == '%')
		info->data_len = 1;
	else
		info->data_len = (info->w_data) ?
		ft_wstrlen(info->w_data) : ft_strlen(info->data);
	info->data_len += ft_strlen(info->add) + ft_strlen(info->presc);
	ft_field(info);
}
