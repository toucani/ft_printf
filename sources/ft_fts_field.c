/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fts_field.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/11 12:43:04 by dkovalch          #+#    #+#             */
/*   Updated: 2017/05/07 10:03:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_printf project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf_inside.h"

/*
**	Creating a string filled with a character we need aka 'field'.
**	It is used in creating string for field width and presicion.
*/

static char		*ft_field_create(size_t amount, const char symbol)
{
	char *rt;

	rt = ft_strnew(amount);
	while (amount)
		rt[--amount] = symbol;
	return (rt);
}

/*
**	Managing presicion.
**	Strings(sS mM):
**		Just shortening the string if we need to.
**	Numbers(dD i uU xX oO bB)
**		Creating a field filled with '0'.
*/

void			ft_presicion(t_conv_info *info)
{
	if (ft_tolower(info->cnv_char) == 's' || ft_tolower(info->cnv_char) == 'm')
	{
		if (ft_strchr(info->args, '.') && info->data_len > info->presc_width)
		{
			if (info->w_data)
				info->w_data[info->presc_width] = 0;
			else
				info->data[info->presc_width] = 0;
		}
	}
	else
	{
		if (info->data_len < info->presc_width)
		{
			info->presc =
				ft_field_create(info->presc_width - info->data_len, '0');
			if (ft_strnequ(info->add, info->presc, ft_strlen(info->add)))
				ft_strdel(&(info->add));
		}
	}
}

/*
**	Field width managment function.
**	Returns 0 is there is nothing we nedd to add when printing
**	zr -> special point to look for a zero as a flag
**	that makes a filed 0000000042 instead of _________42
*/

void			ft_field(t_conv_info *info)
{
	char			*zr[2];

	if (info->data_len >= info->field_width)
		return ;
	zr[0] = 0;
	zr[1] = info->args;
	while (!zr[0] && (zr[1] = ft_strchr(zr[1], '0')))
		if (*(zr[1] - 1) && ft_isdigit(*(zr[1] - 1)))
			zr[1]++;
		else
			zr[0] = zr[1];
	info->field = ft_field_create(info->field_width - info->data_len,
		(zr[0] && (info->cnv_char == 'c' ||
		(ft_tolower(info->cnv_char) == 's' && ft_strchr(info->args, '.'))
		|| !ft_strchr(info->args, '.'))
		&& !ft_strchr(info->args, '-')) ? '0' : ' ');
}
