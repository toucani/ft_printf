# ft_printf

It is my version of printf. It accepts
*i, d, u, U, o, O, x, X, p, c, C, s, S, m, M, b, r* conversions, with
*ll, l, hh, h* flags.

ft_printf can *check errno*, *do binary output* and *print colors*.
For more information - check [ft_printf.h](includes/ft_printf.h).


## Installing

Clone this repository recursively:
```
git clone --recurse-submodules
```
, move into the folder
```
cd ft_printf && mkdir build && cd build
```
and
```
cmake .. && make
```
Run
```
make test
```
to execute unit tests.

## Using

This library can be added as a subdirectory to your cmake project(libraries name is _ftprintf_).
It also can be build with sanitizers, just by adding one
of the following lines to your cmake configuration: 
```
-DSANITIZE_ADDRESS=On
-DSANITIZE_MEMORY=On
-DSANITIZE_THREAD=On
-DSANITIZE_UNDEFINED=On
```

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=ft_printf from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to [PVS Studio](https://www.viva64.com/en/pvs-studio/) which showed me my mistakes.
* to [sanitizers for cmake](https://github.com/arsenm/sanitizers-cmake/)
* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
